﻿namespace FizzBuzz.Interfaces
{
    public interface IFizzBuzzReturnValue
    {
        string Fizz { get; }
        string Buzz { get; }
        string FizzBuzz { get; }

    }
}
