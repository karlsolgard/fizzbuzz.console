﻿using System.Collections.Generic;

namespace FizzBuzz.Interfaces
{
    public interface IFizzBuzzProvider
    {
        IEnumerable<string> GetFizzBuzzCollection(int size);
        bool IsDividableByThree(int value);
        bool IsDividableByFive(int value);
    }
}
