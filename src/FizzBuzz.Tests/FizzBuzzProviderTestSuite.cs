﻿using NUnit.Framework;
using FizzBuzz.Implementation;
using FizzBuzz.Interfaces;
using System.Collections.Generic;

namespace FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzProviderTestSuite
    {
        IFizzBuzzProvider _provider;

        [SetUp]
        public void Init()
        {
            _provider = new FizzBuzzProvider();
        }

        [Test]
        public void ShouldBeDividableByFive()
        {
            var x = _provider.IsDividableByFive(5);
            Assert.IsTrue(x);
        }

        [Test]
        public void ShouldBeDividableByThree()
        {
            var x = _provider.IsDividableByThree(3);
            Assert.IsTrue(x);
        }

        [Test]
        public void ShouldBeDividableByThreeAndFive()
        {
            var x = _provider.IsDividableByThree(15);
            var y = _provider.IsDividableByFive(15);
            Assert.IsTrue(x);
            Assert.IsTrue(y);
        }

        [Test]
        public void ShouldNotBeDividableByFive()
        {
            var x = _provider.IsDividableByFive(8);
            Assert.IsFalse(x);
        }

        [Test]
        public void ShouldNotBeDividableByThree()
        {
            var x = _provider.IsDividableByThree(2);
            Assert.IsFalse(x);
        }

        [Test]
        public void ShouldReturnExpectedCollection()
        {
            var could = _provider.GetFizzBuzzCollection(15);
            var should = new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz" };
            Assert.AreEqual(could, should);
        }
    }
}
