﻿using System.Collections.Generic;
using FizzBuzz.Interfaces;

namespace FizzBuzz.Implementation
{
    public class FizzBuzzProvider : IFizzBuzzProvider
    {
        private readonly IFizzBuzzReturnValue _fizzBuzzReturnValue;

        public FizzBuzzProvider()
        {
            _fizzBuzzReturnValue = new FizzBuzzReturnValue();
        }

        //adding this to support other variations of the FizzBuzz™ game
        public FizzBuzzProvider(IFizzBuzzReturnValue fb)
        {
            _fizzBuzzReturnValue = fb;
        }

        public IEnumerable<string> GetFizzBuzzCollection(int size)
        {
            var fizzbuzzlist = new List<string>();

            for (var i = 1; i <= size; i++)
            {
                if (IsDividableByFive(i) && IsDividableByThree(i))
                {
                    fizzbuzzlist.Add(_fizzBuzzReturnValue.FizzBuzz);
                    continue;
                }

                if (IsDividableByFive(i))
                {
                    fizzbuzzlist.Add(_fizzBuzzReturnValue.Buzz);
                    continue;
                }

                if (IsDividableByThree(i))
                {
                    fizzbuzzlist.Add(_fizzBuzzReturnValue.Fizz);
                    continue;
                }

                fizzbuzzlist.Add(i.ToString());
            }

            return fizzbuzzlist;
        }
        public bool IsDividableByFive(int value)
        {
            return value % 5 == 0;
        }
        
        public bool IsDividableByThree(int value)
        {
            return value % 3 == 0;
        }
    }
}
