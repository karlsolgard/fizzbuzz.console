﻿using System;
using FizzBuzz.Interfaces;

namespace FizzBuzz.Implementation
{
    internal class FizzBuzzReturnValue : IFizzBuzzReturnValue
    {
        public string Buzz => "Buzz";

        public string Fizz => "Fizz";

        public string FizzBuzz => "FizzBuzz";
    }
}