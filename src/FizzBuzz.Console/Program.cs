﻿using FizzBuzz.Implementation;

namespace FizzBuzz.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var fizzBuzzProvider = new FizzBuzzProvider();

            foreach (var result in fizzBuzzProvider.GetFizzBuzzCollection(100))
            {
                System.Console.WriteLine(result);
            }
            System.Console.ReadLine();
        }
    }
}
